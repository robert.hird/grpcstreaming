module source.vivint.com/pl/grpc-streaming

go 1.20

require (
	github.com/gogo/protobuf v1.3.2
	google.golang.org/grpc v1.55.0
	source.vivint.com/pl/log v1.0.6
)

require (
	github.com/golang/mock v1.5.0 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sirupsen/logrus v1.9.0 // indirect
	golang.org/x/net v0.8.0 // indirect
	golang.org/x/sys v0.6.0 // indirect
	golang.org/x/text v0.8.0 // indirect
	google.golang.org/genproto v0.0.0-20230306155012-7f2fa6fef1f4 // indirect
	google.golang.org/protobuf v1.30.0 // indirect
	source.vivint.com/pl/flag v1.5.10 // indirect
)
