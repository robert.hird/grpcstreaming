package main

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	pb "source.vivint.com/pl/grpc-streaming/generated"
	"source.vivint.com/pl/log"
	"time"
)

func main() {
	// Create our context
	ctx := context.Background()
	// Setup connection
	conn, err := grpc.Dial(":50051", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatal("Failed to dial gRPC port", log.Fields{"err": err})
	}
	// Close connection when we are done
	defer conn.Close()
	// Use the generated NewStreamingServiceClient method and pass our Connection
	client := pb.NewStreamingServiceClient(conn)

	req := &pb.SubscribeRequest{
		SystemId: 12345,
		Topic:    "locks",
		UserId:   "uid1",
	}
	// Call Monitor to receive the Stream of data
	stream, err := client.Subscribe(ctx, req)
	if err != nil {
		log.Fatal("Failed to subscribe", log.Fields{"err": err})
	}

	// Create a timer to cancel
	stop := time.NewTicker(7 * time.Minute)
	// Iterate stream
	for {
		select {
		case <-stop.C:
			// Tell the Server to close this Stream, used to clean up running on the server
			err = stream.CloseSend()
			if err != nil {
				log.Fatal("Failed to close stream:", log.Fields{"err": err})
			}
			return
		default:
			// Recieve on the stream
			res, err := stream.Recv()
			if err != nil {
				panic(err)
			}
			fmt.Println("New PubNub Received")
			fmt.Println("Message: ", res.GetMessage())
		}
	}
}
