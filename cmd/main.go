package main

import (
	"context"
	"log"
	"net"

	"google.golang.org/grpc"

	pb "source.vivint.com/pl/grpc-streaming/generated" // Import the generated code
	"source.vivint.com/pl/grpc-streaming/server"
)

func main() {
	listen, err := net.Listen("tcp", ":50051")
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	srv := grpc.NewServer()
	pb.RegisterStreamingServiceServer(srv, server.NewServer(context.Background()))

	log.Println("Server started on port 50051...")
	if err := srv.Serve(listen); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}
}
