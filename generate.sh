#!/bin/bash

set -e

protoc.sh \
        --gogo_out=plugins=grpc,paths=source_relative:generated \
        pubsub.proto