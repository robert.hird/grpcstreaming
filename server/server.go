package server

import (
	"context"
	"fmt"
	"time"

	pb "source.vivint.com/pl/grpc-streaming/generated"
)

type streamer struct {
	stream       pb.StreamingService_SubscribeServer
	lastAccessed time.Time
}

// <pan_id>:<topic>:<user_id>
type key string

type Server struct {
	Subscriptions map[key]streamer
}

func NewServer(ctx context.Context) *Server {
	subscriptions := make(map[key]streamer, 0)
	server := &Server{
		Subscriptions: subscriptions,
	}
	go unregisterExpiredSubscribers(ctx, 1*time.Minute, 6*time.Minute, server)
	return server
}

func getKey(sysID int64, topic, uid string) key {
	return key(fmt.Sprintf("%d:%s:%s", sysID, topic, uid))
}

func (s *Server) Subscribe(req *pb.SubscribeRequest, stream pb.StreamingService_SubscribeServer) error {
	// Start a ticker that executes each 2 seconds
	timer := time.NewTicker(2 * time.Second)

	for {
		select {
		// Exit on stream context done
		case <-stream.Context().Done():
			return nil
		case <-timer.C:
			// Send the Hardware stats on the stream
			err := stream.Send(&pb.SubscribeResponse{
				Message: "Hello",
			})
			if err != nil {
				return err
			}
		}
	}
}

// Real Implementation would need to consider locking RW access to the map.
func unregisterExpiredSubscribers(ctx context.Context, checkFrequency, maxTTL time.Duration, s *Server) {
	ticker := time.NewTicker(checkFrequency)
	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			checkSubscribersToExpire(maxTTL, s)
		case <-ctx.Done():
			return
		}
	}
}

func checkSubscribersToExpire(maxTTL time.Duration, s *Server) {
	for k, v := range s.Subscriptions {
		if time.Since(v.lastAccessed) > maxTTL {
			delete(s.Subscriptions, k)
		}
	}
}
