package grpcstreaming

// run the generate.sh script using the gobuild docker container to generate mocks
//go:generate go mod vendor
//go:generate bash -c "docker run --pull always --rm -v ~/.cache/go-build:/.cache/go-build  -u $(id -u):$(id -g) -v $(pwd):/app -w /app tp-artifactory.vivint.com:5000/gobuild:1.20 ./generate.sh"
